# Instalasi di Windows

1. Download file instalasi [di sini](https://download.biolab.si/download/files/Orange3-3.35.0-Miniconda-x86_64.exe)
2. Klik dua kali pada file instalasi untuk menjalankan instalasi
3. Proses instalasi akan meminta Kamu menginstall Miniconda sebagai komponen dasar aplikasi Orange
4. Finish
