Demistifying Machine Learning With Orange

# Machine Learning

## Era Data Mining (DM) dan Artificial Intelligence (AI)

### Berbagai Contoh Aplikasi Data Mining Saat ini
- Tujuan DM, fokus ke mendapatkan pengetahuan dari berbagai data untuk rekomendasi kebijakan, contohnya pemerintah, direksi perusahaan
- Erat kaitan dengan Big Data
- Bagaimana berbagai ilmu terkait data dapat diterapkan untuk menarik pengetahuan dari data
- Ilustrasi analogi najong dari DM untuk anak kelas 5 SD
- Orang2 yang fokus di bidang ini contohnya: Data Scientist, Data Analyst, Business Intelligence Analyst

### Berbagai Contoh Aplikasi AI Saat Ini 
- Tujuan AI, fokus ke menghadirkan kecerdasan manusia di alat, contohnya operator, otomasi industri
- Bagaimana menghadirkan kecerdasan manusia pada alat
- Ilustrasi analogi najong dari AI untuk anak kelas 5 SD
- Contoh otomasi:
Kategori Kesehatan
Kategori Wabah
Kategori Kebencanaan 
Kategori Lingkungan
Kategori Keamanan
Kategori Industri dan UMKM
  - Mengklasifikasikan wajah 
  - Mengubah suara menjadi teks
  - Membuat musik dan lirik 
  - Membuat gambar dari teks
  - Mengetahui objek-objek di jalan untuk mobil pintar
  - Mendeteksi kantuk pada supir
  - Deteksi penyakit pada kulit
- Orang2 yang fokus di bidang ini contohnya: AI Engineer, ML Engineer, AI Scientist
- Algoritmanya lebih kompleks dari data mining
- Umumnya untuk kebutuhan real time

## Bagaimana algoritma DM dan AI bekerja ?
- By rules
- By learning

## Machine Learning, Membuat Aturan Berdasarkan Pengalaman
- Ilustrasi analogi najong dari machine learning untuk anak kelas 5 SD

#### Discriminative, Membedakan Data Input
- Ilustrasi analogi najong dari discriminative machine learning untuk anak kelas 5 SD

#### Generative, Membuat Data Baru
- Ilustrasi analogi najong dari generative machine learning untuk anak kelas 5 SD

# Demonstrasi Orange Data Mining (00:10:00 - 01:10:00)

## Data Mining Tanpa Coding Dengan Visual Programming

## Mudah Mengakses Data

## Mudah Mentransformasi Data

## Mudah Memvisualisasikan Data

## Mudah Mengklasifikasi Data

## Mudah Memprediksi Data

## Mudah Menganalisis Text

## Mudah Menganalisis Image

## Mudah Menganalisis Data Spasial

# Diskusi  (01:10:00 - 01:30:00)

# Credits
- [Orange3](https://github.com/biolab/orange3)
- [ScreenToGif](https://github.com/NickeManarin/ScreenToGif)
