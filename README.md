# Orange Data Mining - 2023 

## Outline
1. Pengenalan tentang Machine Learning
2. Peran Orange sebagai alat software untuk Machine Learning
3. Mengimpor dan mengeksplorasi data dengan Orange
4. Preprocessing Data
   - Pembersihan data dan penanganan missing values
   - Transformasi data dan normalisasi
   - Pemilihan fitur (feature selection)
5. Model Pembelajaran Mesin
   - Pengenalan tentang model pembelajaran mesin
   - Klasifikasi dengan metode Decision Trees
   - Regresi linier dan non-linier
   - Klasifikasi menggunakan Naive Bayes
6. Clustering dengan K-Means
7. Evaluasi dan Validasi Model
   - Metrik evaluasi dalam Machine Learning
   - Menerapkan validasi silang (cross-validation)
   - Memahami kurva pembelajaran (learning curves)
8. Optimisasi Model
   - Pemilihan parameter dengan grid search
   - Regresi logistik dan SVM
   - Ensemble Learning dan Random Forests
9. Visualisasi dan Interpretasi Hasil
   - Visualisasi data dengan Orange
   - Interpretasi hasil model pembelajaran mesin
   - Menyajikan laporan dan hasil prediksi
10.Deployment dan Pemeliharaan Model
   - Mengekspor model pembelajaran mesin
   - Integrasi model dengan aplikasi lain
   - Pemeliharaan dan pengoptimalan model
11. Studi Kasus dan Latihan
12. Menyelesaikan studi kasus menggunakan dataset nyata
13. Latihan dan proyek mini untuk memperkuat pemahaman
