# Katalog Widget

Sumber: [Katalog widget](https://orangedatamining.com/widget-catalog/)

✅: Widget telah tersedia

🟨: Widget perlu diinstall terlebih dahulu via menu Options -> Add-ons

---

## ✅ Data - Akses Data 
- ![](https://orangedatamining.com/widget-icons/Data-File.png){width=40} File : Mengambil data dari file .xlsx, .txt, .csv
- ![](https://orangedatamining.com/widget-icons/Data-CSV%20File%20Import.png){width=40} CSV File Import : Mengambil data dari file .csv
- ![](https://orangedatamining.com/widget-icons/Data-Datasets.png){width=40} Datasets : Download dataset dari sumber online
- ![](https://orangedatamining.com/widget-icons/Data-SQL%20Table.png){width=40} SQL Table : Mengambil dataset dari database PostgreSQL atau SQL Server. Memerlukan instalasi add-on
- ![](https://orangedatamining.com/widget-icons/Data-Data%20Table.png){width=40} Data Table : Menampilkan dan memilih data dari sumber data



## ✅ Transform - Transformasi Data

## ✅ Visualize - Visualisasi Data

## Pemodelan Pemodelan Machine Learning

### ✅ Model - Pemodelan Supervised Machine Learning

### ✅ Unsupervised - Pemodelan Unsupervised Machine Learning

### 🟨 Associate - Pemodelan Asosiasi Data

### 🟨 Explain - Penjelasan Model dan Fitur

### 🟨 Educational - Penjelasan Model Secara Visual

## ✅ Evaluate - Evaluasi Model Machine Learning

## Analisis Bentuk Data Spesifik

### 🟨 Time Series - Pemrosesan dan Analisis Data Time Series

### 🟨 Text Mining - Pengetahuan dan Analisis Teks

### 🟨 Image Analytics - Pemrosesan dan Analisis Image

### 🟨 Network - Pemrosesan dan Analisis Network

### 🟨 Geo - Pemrosesan dan Analisis Data Spasial

## Analisis Bidang Spesifik

### 🟨 Spectroscopy - Analisis Data Spektrum

### 🟨 Survival Analysis - Analisis Data Kesehatan - Survival

### 🟨 Bioinformatics - Analisis Data Biologi

